#include <iostream>
#include <string>
#include "similarwords.h"
#include "tester.h"
#include "profiler.h"

void TestExample()
{
    SimilarWordsDictionary dic;
    dic.Create("prosoft", "c++");
    ASSERT_EQUAL(dic.Quantity("school"), 0);
    dic.Create("c++", "school");
    ASSERT_EQUAL(dic.Quantity("c++"), 2);
    ASSERT_EQUAL(dic.Quantity("prosoft"), 1);
    ASSERT_EQUAL(dic.CheckSimilar("prosoft", "c++"), "YES");
    ASSERT_EQUAL(dic.CheckSimilar("prosoft", "school"), "NO");
    ASSERT_EQUAL(dic.CheckSimilar("hello", "world"), "NO");
}

void TestTransitivnost()
{
    SimilarWordsDictionary dic;
    dic.Create("A", "B");
    dic.Create("B", "C");
    dic.Create("C", "D");
    ASSERT_EQUAL(dic.CheckSimilar("A", "C"), "NO");
    ASSERT_EQUAL(dic.CheckSimilar("A", "D"), "NO");
    ASSERT_EQUAL(dic.CheckSimilar("B", "D"), "NO");
}

void Test_1()
{
    SimilarWordsDictionary dic;
    dic.Create("A", "B");
    dic.Create("A", "B");
    ASSERT_EQUAL(dic.CheckSimilar("A", "B"), "YES");
    ASSERT_EQUAL(dic.Quantity("A"), 1);
    ASSERT_EQUAL(dic.Quantity("B"), 1);
}

void Test_2()
{
    SimilarWordsDictionary dic;
    ASSERT_EQUAL(dic.Quantity("prosoft"), 0);
    dic.Create("program", "code");
    dic.Create("program", "code1");
    dic.Create("program", "code2");
    dic.Create("program", "code3");
    dic.Create("program", "code4");
    dic.Create("program", "code5");
    ASSERT_EQUAL(dic.Quantity("program"), 6);
}

void Test_3()
{
    SimilarWordsDictionary dic;
    ASSERT_EQUAL(dic.CheckSimilar("Rust", "Lisp"), "NO");
    dic.Create("c++", "java");
    dic.Create("c++", "python");
    ASSERT_EQUAL(dic.Quantity("RUST"), 0);
}

void Test_4()
{
    SimilarWordsDictionary dic;
    ASSERT_EQUAL(dic.Quantity("RUST"), 0);
    dic.Create("c++", "java");
    dic.Create("c++", "python");
    ASSERT_EQUAL(dic.Quantity("RUST"), 0);
}

void Test_5()
{
    SimilarWordsDictionary dic;
    ASSERT_EQUAL(dic.CheckSimilar("Rust", "Lisp"), "NO");
    ASSERT_EQUAL(dic.CheckSimilar("Rust", "Lisp"), "NO");
    ASSERT_EQUAL(dic.CheckSimilar("Rust", "Lisp"), "NO");
    ASSERT_EQUAL(dic.CheckSimilar("Rust", "Lisp"), "NO");
    ASSERT_EQUAL(dic.Quantity("Rust"), 0);
    dic.Create("Rust", "c++");
    ASSERT_EQUAL(dic.CheckSimilar("Rust", "Lisp"), "NO");
    ASSERT_EQUAL(dic.CheckSimilar("Rust", "Lisp"), "NO");
    ASSERT_EQUAL(dic.Quantity("Rust"), 1);
    ASSERT_EQUAL(dic.Quantity("Lisp"), 0);
    ASSERT_EQUAL(dic.Quantity("Lisp"), 0);
    ASSERT_EQUAL(dic.Quantity("Lisp"), 0);

}

void Test_6()
{
//    ASSERT(false);
    SimilarWordsDictionary dic;
    dic.Create("A", "B");
    dic.Create("A", "B");
    dic.Create("A", "C");
    dic.Create("A", "C");
    dic.Create("A", "D");
    ASSERT_EQUAL(dic.Quantity("A"), 3);
}

void Test_7()
{
    SimilarWordsDictionary dic;
    dic.Create("prosoft", "siemens");
    ASSERT_EQUAL(dic.Quantity("prosoft"), 1);
    ASSERT_EQUAL(dic.Quantity("siemens"), 1);
    dic.Create("siemens", "prosoft");
    ASSERT_EQUAL(dic.Quantity("prosoft"), 1);
    ASSERT_EQUAL(dic.Quantity("siemens"), 1);
}

void TestCheckSimilar()
{
    SimilarWordsDictionary dic;
    dic.Create("c++", "code");
    ASSERT_EQUAL(dic.CheckSimilar("c++", "code"), "YES");
    ASSERT_EQUAL(dic.CheckSimilar("code", "c++"), "YES");
    dic.Create("c++", "program");
    ASSERT_EQUAL(dic.CheckSimilar("c++", "program"), "YES");
    ASSERT_EQUAL(dic.CheckSimilar("program", "c++"), "YES");
}

constexpr int maxQ = 25'000;
void mainFunc(size_t q, istream& is);

void TestBenchmark(int q, istream& is, int max)
{
    int dur = 0;
    {
        LOG_DURATION(dur, "")
        mainFunc(q, is);
    }
    cerr << "DURATION: " << dur << " / " << max << endl;
    if (dur > max)
    {
        stringstream ss;
        ss << dur << " / " << max << " time limit exceed" << endl;
        Assert(false, ss.str());
    }
}

void Test_Highload_1()
{
    stringstream ss;
    for (int i = 0; i < maxQ * 3; i += 3)
    {
        ss << "CREATE " << to_string(i) << " " << to_string(i + 1) << std::endl;
    }
    TestBenchmark(maxQ, ss, 100);
}

void Test_Highload_2()
{
//    ASSERT(false);
    {
        stringstream ss;
        for (int i = 0; i < maxQ; ++i)
        {
            ss << "CREATE prosoft google" << std::endl;
        }
        TestBenchmark(maxQ, ss, 20);
    }
    {
        stringstream ss;
        for (int i = 0; i < maxQ; ++i)
        {
            ss << "CREATE prosoft " << to_string(i) << std::endl;
        }
        TestBenchmark(maxQ, ss, 90);
    }
}

void Test_Highload_3()
{
    {
        stringstream ss;
        int i = 0;
        for (; i < maxQ / 2; ++i)
        {
            ss << "CREATE " << to_string(maxQ - i) << " " << to_string(i) << std::endl;
        }
        for (; i < maxQ; ++i)
        {
            ss << "QUANTITY " << to_string(maxQ) << " " << to_string(0) << endl;
        }
        TestBenchmark(maxQ, ss, 50);
    }
    {
        stringstream ss;
        int i = 0;
        for (; i < maxQ / 2; ++i)
        {
            ss << "CREATE prosoft" << " " << to_string(i) << std::endl;
        }
        for (; i < maxQ; ++i)
        {
            ss << "QUANTITY prosoft" << endl;
        }
        TestBenchmark(maxQ, ss, 60);
    }
    {
        stringstream ss;
        int i = 0;
        ss << "CREATE " << to_string(maxQ) << " " << to_string(0) << std::endl;
        for (; i < maxQ; ++i)
        {
            ss << "QUANTITY " << to_string(maxQ) << " " << to_string(0) << endl;
        }
        TestBenchmark(maxQ, ss, 10);
    }
}

void Test_Highload_4()
{
    {
        stringstream ss;
        int i = 0;
        for (; i < maxQ / 2; ++i)
        {
            ss << "CREATE " << to_string(maxQ - i) << " " << to_string(i) << std::endl;
        }
        for (; i < maxQ; ++i)
        {
            ss << "CHECKSIMILAR " << to_string(maxQ) << " " << to_string(0) << endl;
        }
        TestBenchmark(maxQ, ss, 60);
    }
    {
        stringstream ss;
        int i = 0;
        for (; i < maxQ / 2; i += 2)
        {
            ss << "CREATE prosoft " << to_string(i) << std::endl;
        }
        for (; i < maxQ; ++i)
        {
            ss << "CHECKSIMILAR prosoft " << to_string(i) << endl;
        }
        TestBenchmark(maxQ, ss, 45);
    }
    {
        stringstream ss;
        int i = 0;
        ss << "CREATE " << to_string(maxQ) << " " << to_string(0) << std::endl;
        for (; i < maxQ; ++i)
        {
            ss << "CHECKSIMILAR " << to_string(maxQ) << " " << to_string(0) << endl;
        }
        TestBenchmark(maxQ, ss, 15);
    }
}

void Test_HighLoad_5()
{
    stringstream ss;
    int i = 0;
    for (; i < maxQ / 3; ++i)
    {
        ss << "CREATE " << to_string(maxQ - i) << " " << to_string(i) << std::endl;
    }
    for (; i < maxQ / 3; ++i)
    {
        ss << "QUANTITY " << to_string(maxQ - i) << " " << to_string(i) << endl;
    }
    for (; i < maxQ / 3; ++i)
    {
        ss << "CHECKSIMILAR " << to_string(maxQ - i) << " " << to_string(i) << endl;
    }
    TestBenchmark(maxQ, ss, 40);
}

void mainFunc(size_t q, istream& is)
{
    SimilarWordsDictionary dictionary;
    stringstream tempCout;

    for (size_t i = 0; i < q; i++)
    {
        std::string operation;
        is >> operation;

        if (operation == "CREATE")
        {
            std::string word1;
            is >> word1;

            std::string word2;
            is >> word2;
            dictionary.Create(word1, word2);
        }
        else if (operation == "QUANTITY")
        {
            std::string word;
            is >> word;

            tempCout << dictionary.Quantity(word) << std::endl;
        }
        else if (operation == "CHECKSIMILAR")
        {
            std::string word1;
            is >> word1;

            std::string word2;
            is >> word2;

            tempCout << dictionary.CheckSimilar(word1, word2) << std::endl;
        }
    }
}

int main()
{
    TestRunner tr;
    RUN_TEST(tr, TestExample); //test_example
    RUN_TEST(tr, Test_1); // IdenticalSynonyms
    RUN_TEST(tr, Test_2); // Quantity1
    RUN_TEST(tr, Test_3);
    RUN_TEST(tr, Test_4);
    RUN_TEST(tr, Test_5);
    RUN_TEST(tr, Test_6); // Quantity2
    RUN_TEST(tr, Test_7); // viseVersa
    RUN_TEST(tr, Test_Highload_1); // Create
    RUN_TEST(tr, Test_Highload_2); // CreateIdentity
    RUN_TEST(tr, Test_Highload_3); // Quantity
    RUN_TEST(tr, Test_Highload_4); // CheckSimilar
    RUN_TEST(tr, Test_HighLoad_5); // general highload
    return 0;
}
