#pragma once

#include <chrono>
#include <iostream>
#include <string>

using namespace std;
using namespace std::chrono;

class LogDuration {
public:
    explicit LogDuration(int& dur, const string& msg = "")
        : m_dur(dur),
          message(msg + ": "),
          start(steady_clock::now())
    {
    }

    ~LogDuration() {
        auto finish = steady_clock::now();
        auto dur = finish - start;
        m_dur = duration_cast<milliseconds>(dur).count();
//        cerr << message << m_dur << " ms" << endl;
    }
private:
    int& m_dur;
    string message;
    steady_clock::time_point start;
};

#define UNIQ_ID_IMPL(lineno) _a_local_var_##lineno
#define UNIQ_ID(lineno) UNIQ_ID_IMPL(lineno)

#define LOG_DURATION(dur, message) \
  LogDuration UNIQ_ID(__LINE__)(dur, message);
